# Expert system for diagnosing diabetes in children

## Installation

1. First of all, clone this project `git clone git@gitlab.com:angelokezimana/expert-system.git`;
2. Navigate to the root project folder `cd expert-system`;
3. Activate the Python virtual environment `env\Scripts\activate`
4. Install the dependencies of this project `pip install -r requirements.txt`;
5. Run the project `py main.py`.
